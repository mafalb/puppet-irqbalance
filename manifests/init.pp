#-
# Copyright (c) 2012 Markus Falb <markus.falb@fasel.at>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Enable (or disable) the irqbalance daemon.
#
# hasstatus has to be present in the service definition, it seems.
# I dont understand why, though
# http://groups.google.com/group/puppet-users/browse_thread/thread/ba5d40fa14a2719f/8c5214cd92393bd2?#8c5214cd92393bd2
#
class irqbalance {

  include irqbalance::data
  
  # It does not make sense to balance between only one core...
  # ...uninstall irqbalance, its not useful for us.
  if $processorcount == 1 {
    package { 'irqbalance': ensure => absent }
  }

  # We have multiple cores...
  # ...install irqbalance
  else {
    package { 'irqbalance': }
    service { 'irqbalance':
      ensure => running,
      enable => true,
      hasstatus => true,
      require => Package['irqbalance'],
    }
  }
}
