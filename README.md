# puppet irqbalance module

Enable (or disable) the irqbalance daemon [http://irqbalance.org][1]  

if there is only one core -> disable  
multiple cores -> enable  

If this module is used on an OS untested with than a failure should occure
`The irqbalance module is not tested with your OS`
(or similar)
If you encounter such an error modify data.pp if appropiate and report back, please.

The web page for contributing is at [https://bitbucket.org/mafalb/puppet-irqbalance][2]

## Usage

    include irqbalance

[1]: http://irqbalance.org
[2]: https://bitbucket.org/mafalb/puppet-irqbalance